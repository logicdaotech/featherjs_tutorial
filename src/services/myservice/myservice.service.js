// Initializes the `myservice` service on path `/myservice`
const createService = require('feathers-nedb');
const createModel = require('../../models/myservice.model');
const hooks = require('./myservice.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'myservice',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/myservice', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('myservice');

  service.hooks(hooks);
};
