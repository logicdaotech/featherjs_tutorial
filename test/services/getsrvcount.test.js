const assert = require('assert');
const app = require('../../src/app');

describe('\'getsrvcount\' service', () => {
  it('registered the service', () => {
    const service = app.service('getsrvcount');

    assert.ok(service, 'Registered the service');
  });
});
