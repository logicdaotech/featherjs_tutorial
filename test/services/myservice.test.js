const assert = require('assert');
const app = require('../../src/app');

describe('\'myservice\' service', () => {
  it('registered the service', () => {
    const service = app.service('myservice');

    assert.ok(service, 'Registered the service');
  });
});
